import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmployeeService} from "../services/employee.service";
import {Observable} from "rxjs";
import {EmployeeDetails, EmployeeInterface} from "@models/interfaces/employee.interface";
import {map} from "rxjs/operators";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'ema-employee-listing',
  templateUrl: './employee-listing.component.html',
  styleUrls: ['./employee-listing.component.scss']
})
export class EmployeeListingComponent implements OnInit, OnDestroy {
  searchTerm: string;
  employees: EmployeeDetails[];
  constructor(private _empService: EmployeeService, private _fb: FormBuilder) {
  }

  ngOnInit(): void {
    this._empService.loadEmployees();
    this._empService.employees$
      .pipe(
        map(res => res.sort((a: any, b: any) => a.FirstName > b.FirstName ? 1 : -1))
      )
      .subscribe(
        res => {
          this.employees = res
        }
      )
  }

  ngOnDestroy() {

  }

}
