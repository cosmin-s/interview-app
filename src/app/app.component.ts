import {AfterContentInit, AfterViewInit, Component, ViewChild} from '@angular/core';
import {ModalHostDirective} from "./directives/modal-host.directive";

@Component({
  selector: 'ema-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'em-app';

}
