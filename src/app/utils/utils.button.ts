import {ButtonFlavour, ButtonType} from "@models/enums/button.enums";

export class UtilsButton {
  static get ButtonFlavour() {
    return ButtonFlavour
  }

  static get ButtonType() {
    return ButtonType
  }
}
