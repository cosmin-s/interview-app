import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {EmployeeDetails} from "@models/interfaces/employee.interface";
import {ModalService} from "@shared/modal/modal.service";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private subject = new BehaviorSubject<EmployeeDetails[]>([]);
  employees$: Observable<EmployeeDetails[]> = this.subject.asObservable();

  constructor(private http: HttpClient, private _modalService: ModalService) {
  }

  loadEmployees(): boolean {
    const currentStoredEmployees = this.getCurrentEmployees() || [];
    if (currentStoredEmployees.length) {
      this.pushLatestEmployees(currentStoredEmployees);
      return false;
    }
    this.http.get<EmployeeDetails[]>('assets/data/employees.json').subscribe(
      employees => {
        this.saveEmployees(employees);
        this.pushLatestEmployees(employees);
      }
    );
    return true;
  }

  delete(empId: string) {
    const currentEmployees = this.getCurrentEmployees();
    const newEmployees = currentEmployees.filter((emp: EmployeeDetails) => emp.Id !== empId);
    this.saveEmployees(newEmployees);
    this.pushLatestEmployees(newEmployees);
  }

  update(employee: EmployeeDetails, changes: {}) {
    const currentEmployees = this.getCurrentEmployees();
    const editedEmployeeIdx = currentEmployees.findIndex((emp: EmployeeDetails) => emp.Id === employee.Id);
    const newEmployees = [...currentEmployees];
    newEmployees[editedEmployeeIdx] = {
      ...newEmployees[editedEmployeeIdx],
      ...changes
    }
    this.saveEmployees(newEmployees)
      .subscribe( _ => this.pushLatestEmployees(newEmployees));
  }

  getEmployeeById(empId: string) {
    const emp = this.getCurrentEmployees().find((emp:EmployeeDetails) => emp.Id === empId) as EmployeeDetails;
    return emp
  }

  pushLatestEmployees(employees: EmployeeDetails[]) {
    this.subject.next(employees);
  }

  saveEmployees(employees: EmployeeDetails[]) {
    localStorage.setItem('employees', JSON.stringify(employees));
    return new Observable();
  }

  getCurrentEmployees() {
    return JSON.parse((localStorage.getItem('employees') || null) as string)
  }
}
