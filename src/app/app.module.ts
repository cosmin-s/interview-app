import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "@shared/shared.module";
import {EmployeeListingComponent} from './employee-listing/employee-listing.component';
import {EmployeeItemComponent} from './employee-item/employee-item.component';
import {HeaderComponent} from '@shared/header/header.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SearchPipe} from './pipes/search.pipe';
import { Page404Component } from './page404/page404.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListingComponent,
    EmployeeItemComponent,
    HeaderComponent,
    SearchPipe,
    Page404Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
