import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[emaModalHost]'
})
export class ModalHostDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
