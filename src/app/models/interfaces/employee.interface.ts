export interface EmployeeInterface {
  name: string;
  position: string;
  thumb: string;
}

export interface EmployeeDetails {
  Blocked: boolean;
  Details: string;
  FirstName: string;
  Id: string;
  Position: string;
  SecondName: string;
}
