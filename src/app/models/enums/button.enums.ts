export enum ButtonFlavour {
  Primary='btn-primary',
  Secondary='btn-secondary',
  Success='btn-success',
  Danger='btn-danger',
}

export enum ButtonType {
  IconDelete= 'btn-icon-delete',
  IconEdit= 'btn-icon-edit',
  Text= 'btn-text'
}
