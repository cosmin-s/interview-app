import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EmployeeListingComponent} from "./employee-listing/employee-listing.component";
import {EmployeeEditComponent} from "./employee-edit/employee-edit.component";
import {Page404Component} from "./page404/page404.component";

const routes: Routes = [
  {
    path: '', redirectTo: 'listing', pathMatch: 'full'
  },
  {
    path: 'listing', component: EmployeeListingComponent,
    pathMatch: 'full'
  },
  {
    path: 'edit',
    loadChildren: () => import('./employee-edit/edit.module').then(module => module.EditModule),
    data: {
      preload: true
    }
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
