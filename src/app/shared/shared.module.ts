import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {ButtonComponent} from './button/button.component';
import {ModalComponent} from './modal/modal.component';
import {ModalContainerComponent} from './modal-container/modal-container.component';
import {ModalHostDirective} from "../directives/modal-host.directive";

@NgModule({
  declarations: [
    ButtonComponent,
    ModalComponent,
    ModalContainerComponent,
    ModalHostDirective
  ],
  exports: [
    ButtonComponent,
    ModalComponent,
    ModalContainerComponent,
    ModalHostDirective
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

  ]
})
export class SharedModule {
}
