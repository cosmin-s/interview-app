import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {ButtonFlavour, ButtonType} from "@models/enums/button.enums";
import {UtilsButton} from "../../utils/utils.button";

@Component({
  selector: 'ema-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  utils = UtilsButton;
  btnClass = ButtonFlavour.Primary;
  deleteIcon = 'src/app/assets/images/delete-user.png';
  editIcon = 'src/app/assets/images/edit-user.png';
  @Input()
  btnText = 'Button';
  @Input()
  btnDisabled = false;
  @Input()
  btnFlavour = ButtonFlavour.Primary;
  @Input()
  btnType = ButtonType.Text;
  @Output()
  onClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.btnClass = this.utils.ButtonFlavour.Primary;
  }

}
