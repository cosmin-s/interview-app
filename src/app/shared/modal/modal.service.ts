import {ComponentFactoryResolver, Injectable, ViewChild} from '@angular/core';
import {ModalHostDirective} from "../../directives/modal-host.directive";
import {ModalComponent} from "@shared/modal/modal.component";
import {BehaviorSubject, Subject} from "rxjs";
import {EmployeeDetails} from "@models/interfaces/employee.interface";

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private subjectOpen = new Subject<any>(); // add modal data type
  private subjectConfirm = new Subject<boolean>();

  subjectOpen$ = this.subjectOpen.asObservable();
  subjectConfirm$ = this.subjectConfirm.asObservable();

  constructor() { }

  open(emp: EmployeeDetails) {
    this.subjectOpen.next(emp);
    return this.subjectConfirm$;
  }

  confirm(res: boolean) {
    this.subjectConfirm.next(res);
  }

  destroy() {
    this.subjectConfirm.unsubscribe();
  }

  dismiss() {
  }
}
