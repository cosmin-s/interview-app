import {Component, ComponentFactoryResolver, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UtilsButton} from "../../utils/utils.button";
import {ModalHostDirective} from "../../directives/modal-host.directive";
import {ModalService} from "@shared/modal/modal.service";
import {Subject} from "rxjs";
import {EmployeeDetails} from "@models/interfaces/employee.interface";

@Component({
  selector: 'ema-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  utils = UtilsButton;
  @Input()
  employeeData: EmployeeDetails;

  constructor(private _modalService: ModalService) { }

  ngOnInit(): void {
  }

  ok() {
    this._modalService.confirm(true);
  }

  dismiss() {
    this._modalService.confirm(false);
  }

}
