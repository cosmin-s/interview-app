import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild
} from '@angular/core';
import {ModalHostDirective} from "../../directives/modal-host.directive";
import {ModalComponent} from "@shared/modal/modal.component";
import {ModalService} from "@shared/modal/modal.service";
import {tap} from "rxjs/operators";

@Component({
  selector: 'ema-modal-container',
  templateUrl: './modal-container.component.html',
  styleUrls: ['./modal-container.component.scss']
})
export class ModalContainerComponent implements OnInit {
  @ViewChild(ModalHostDirective, {static: true}) modalHost!: ModalHostDirective

  constructor(
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _modalService: ModalService,
    private _cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
     this._modalService.subjectOpen$
       .pipe(
         tap((data) => {
         this._cdr.markForCheck();
         this.loadModal(data);
       }))
       .subscribe();
    this._modalService.subjectConfirm$
      .pipe(
        tap(()=> {
          this.destroyModal();
        })
      )
      .subscribe();
  }

  loadModal(data?: any) {
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(ModalComponent);
    this.modalHost.viewContainerRef.clear();
    const compRef = this.modalHost.viewContainerRef.createComponent(componentFactory);
    compRef.instance.employeeData = data;
  }

  destroyModal() {
    this.modalHost.viewContainerRef.remove();
  }
}
