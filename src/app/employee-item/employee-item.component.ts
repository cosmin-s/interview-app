import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  ComponentFactoryResolver
} from '@angular/core';
import {EmployeeDetails, EmployeeInterface} from "@models/interfaces/employee.interface";
import {UtilsButton} from "../utils/utils.button";
import {EmployeeService} from "../services/employee.service";
import {ModalService} from "@shared/modal/modal.service";
import {ModalHostDirective} from "../directives/modal-host.directive";
import {ModalComponent} from "@shared/modal/modal.component";
import {Router} from "@angular/router";

@Component({
  selector: 'ema-employee-item',
  templateUrl: './employee-item.component.html',
  styleUrls: ['./employee-item.component.scss']
})
export class EmployeeItemComponent implements OnInit {
  @Input()
  employee: EmployeeDetails = {} as EmployeeDetails;
  btnUtils = UtilsButton;

  constructor(
    private _employeeService: EmployeeService,
    private _modalService: ModalService,
    private _router: Router) {
  }

  ngOnInit(): void {
  }

  delete(employee: EmployeeDetails) {
    const sub = this._modalService.open(employee)
      .subscribe(res => {
        sub.unsubscribe();
        return res && this._employeeService.delete(employee.Id)
      })
  }

  edit(empId: string) {
    this._router.navigate(['edit', empId]).then(()=>{});
  }
}
