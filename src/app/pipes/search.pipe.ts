import { Pipe, PipeTransform } from '@angular/core';
import {EmployeeDetails} from "@models/interfaces/employee.interface";

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: EmployeeDetails[], term: string){
    return items.filter(item => {
      return term ?
        item.FirstName.toLowerCase().includes(term) ||
        item.SecondName.toLowerCase().includes(term) ||
        item.Position.toLowerCase().includes(term) : items
    })
  }

}
