import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EditRoutingModule} from './edit-routing.module';
import {EmployeeEditComponent} from "./employee-edit.component";
import {SharedModule} from "@shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    EmployeeEditComponent
  ],
  imports: [
    CommonModule,
    EditRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class EditModule {
}
