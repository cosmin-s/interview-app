import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EmployeeService} from "../services/employee.service";
import {EmployeeDetails} from "@models/interfaces/employee.interface";
import {FormBuilder, Validators} from "@angular/forms";
import {UtilsButton} from "../utils/utils.button";
import {filter} from "rxjs/operators";

@Component({
  selector: 'ema-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss']
})
export class EmployeeEditComponent implements OnInit {
  btnUtils = UtilsButton;
  employee: EmployeeDetails;
  canBeSaved: boolean;
  form = this._fb.group({
    position: ['', {
      validators: [Validators.required]
    }]
  })

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private _router: Router,
    private _employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
    const empId = this._activatedRoute.snapshot.paramMap.get('id') as string;
    this.employee = this._employeeService.getEmployeeById(empId);
    const initialPosition = this.employee.Position;
    this.form.controls['position'].setValue(initialPosition);
    this.form.valueChanges
      .pipe(
        filter(() => this.form.valid)
      )
      .subscribe(val => {
        this.canBeSaved = initialPosition.toString() != this.position.toString();
      })
  }

  save() {
    this._employeeService.update(this.employee, {Position: this.position});
    this._dismiss();
  }

  get position() {
    return this.form.controls['position'].value;
  }

  cancel() {
    this._dismiss();
  }

  private _dismiss() {
    this._router.navigate(['listing']).then(()=>{});
  }

}
